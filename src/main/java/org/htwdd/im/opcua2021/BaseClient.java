package org.htwdd.im.opcua2021;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Security;
import java.util.concurrent.CompletableFuture;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.eclipse.milo.opcua.sdk.client.OpcUaClient;
import org.eclipse.milo.opcua.sdk.client.api.identity.AnonymousProvider;
import org.eclipse.milo.opcua.stack.core.types.builtin.LocalizedText;
import org.eclipse.milo.opcua.stack.core.types.builtin.unsigned.UInteger;
import org.eclipse.milo.opcua.stack.core.types.structured.EndpointDescription;
import org.slf4j.LoggerFactory;

public class BaseClient {
	private String endpointUrl;
	OpcUaClient client;
	
	static {
        // Required for SecurityPolicy.Aes256_Sha256_RsaPss
        Security.addProvider(new BouncyCastleProvider());
    }
	private final CompletableFuture<OpcUaClient> future = new CompletableFuture<>();
	
	public BaseClient(final String endpointUrl) {
		this.endpointUrl = endpointUrl;
	}
	
	private EndpointDescription updateEndpointUrl(
		    EndpointDescription original, String hostname)  {
		
			try {
				URI uri;
				uri = new URI(original.getEndpointUrl()).parseServerAuthority();
				  
				  return new EndpointDescription(
					        hostname,
					        original.getServer(),
					        original.getServerCertificate(),
					        original.getSecurityMode(),
					        original.getSecurityPolicyUri(),
					        original.getUserIdentityTokens(),
					        original.getTransportProfileUri(),
					        original.getSecurityLevel()
					    );
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return original;
			}

		    
		    
		}

	
	public OpcUaClient createClient() throws Exception {
        Path securityTempDir = Paths.get(System.getProperty("java.io.tmpdir"), "security");
        Files.createDirectories(securityTempDir);
        if (!Files.exists(securityTempDir)) {
            throw new Exception("unable to create security dir: " + securityTempDir);
        }

        LoggerFactory.getLogger(getClass())
            .info("security temp dir: {}", securityTempDir.toAbsolutePath());

        KeyStoreLoader loader = new KeyStoreLoader().load(securityTempDir);

        return OpcUaClient.create(
            endpointUrl,
            endpoints ->
                endpoints.stream().map(ep -> {
            		ep = updateEndpointUrl(ep, endpointUrl);
					System.out.println(ep);
					return ep;
				}).filter(e -> true)
                .findFirst(),
            configBuilder ->
                configBuilder
                    .setApplicationName(LocalizedText.english("eclipse milo opc-ua client"))
                    .setApplicationUri("urn:eclipse:milo:examples:client")
                    .setCertificate(loader.getClientCertificate())
                    .setKeyPair(loader.getClientKeyPair())
                    .setIdentityProvider(new AnonymousProvider())
                    .setRequestTimeout(UInteger.valueOf(5000))
                    .build()
        );
    }
	
	

	
}
