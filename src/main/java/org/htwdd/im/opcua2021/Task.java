package org.htwdd.im.opcua2021;

import java.util.concurrent.CompletableFuture;

import org.eclipse.milo.opcua.sdk.client.OpcUaClient;

public interface Task {
	public void run(OpcUaClient client, CompletableFuture<OpcUaClient> future) throws Exception;
}
