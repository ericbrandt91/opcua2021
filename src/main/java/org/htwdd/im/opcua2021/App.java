package org.htwdd.im.opcua2021;

import org.eclipse.milo.opcua.sdk.client.OpcUaClient;
import org.eclipse.milo.opcua.stack.core.types.builtin.NodeId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Hello world!
 *
 */

 
public class App 
{
	public static String OPCUA_URL = "opc.tcp://192.168.0.207:53530/OPCUA/SimulationServer";
	public static Logger logger = LoggerFactory.getLogger(App.class);
	
	public static void main( String[] args ) throws Exception
    {
		  
		BasicRunner runner = new BasicRunner();
		OpcUaClient client = new BaseClient(OPCUA_URL).createClient();
		
		/**
		 * TODO: implement the following use cases
		 * 
		 */
		/*
		Task subscribe = 
		Task read = 
		Task write = 
		Task method = 
		*/
		
    }
	
	
	
}
